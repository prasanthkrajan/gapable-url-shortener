Rails.application.routes.draw do

  root to: 'urls#new'
  post '/' => 'urls#create'
  get '/show' => 'urls#show'
  get '/:url' => 'urls#redirect', as: :redirect

end
