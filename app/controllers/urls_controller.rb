class UrlsController < ApplicationController
require 'securerandom'

before_filter :validate_url, :only => :create

	def new
		@url = Url.new
	end

	def create
		url = params[:url][:original_url]

		original = Url.find_by(original_url: url)

		if original.nil?
			original = url
			new_url = Url.new
			new_url.original_url = original
			short_url = SecureRandom.hex(6)
			new_url.short_url = short_url
			new_url.save
		else
			short_url = original.short_url
		end

		redirect_to show_path(url: short_url)
	end

	def show
		@shortened_url = params[:url]
	    @target_url = request.host_with_port + '/' + @shortened_url
	end

	def redirect
		shortened_url = params[:url]
		url = Url.find_by(short_url: shortened_url)

		redirect_to url.original_url unless url.nil?
		redirect_to root_path, :flash => { :error => "Invalid URL!" } if url.nil?
	end

	

	def validate_url
		url = params[:url][:original_url]
		parsed = URI.parse(url)
		validated = %w( http https ).include?(parsed.scheme)
		redirect_to root_path, :flash => { :error => "Invalid URL!" } unless validated
	end
end
