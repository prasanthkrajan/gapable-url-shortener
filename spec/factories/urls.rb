FactoryGirl.define do
  factory :url do
    original_url "MyString"
    short_url "MyString"
  end
end
