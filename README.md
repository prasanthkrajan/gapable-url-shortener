# README #

This is a URL Shortener built for Gapable.

The link to a working demo of the project is given below:

[http://gapable.herokuapp.com/](http://gapable.herokuapp.com/)

You can paste any valid url to the textbox in the site and generate a unique shortened url. The generated shortened url redirects to the original url.

This site is built based on existing sites like bit.ly and goo.gl.